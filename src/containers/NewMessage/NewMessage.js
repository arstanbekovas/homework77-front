import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ChatForm from "../../components/ChatForm/ChatForm";
import {createChat, } from "../../store/actions/chats";

class NewMessage extends Component {
  createChat = chatData => {
    this.props.onChatCreated(chatData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>New chat</PageHeader>
        <ChatForm onSubmit={this.createChat} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onChatCreated: chatData => {
      return dispatch(createChat(chatData))
    }
  }
};

export default connect(null, mapDispatchToProps)(NewMessage);