import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Image, PageHeader, Panel} from "react-bootstrap";
import {fetchChat} from "../../store/actions/chats";
import {Link} from "react-router-dom";

class Chats extends Component {
  componentDidMount() {
    this.props.onFetchChat();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Chat
          <Link to="/chat/new">
            <Button bsStyle="primary" className="pull-right">
              Add message
            </Button>
          </Link>
        </PageHeader>

        {this.props.chat.map(chat => (
          <Panel key={chat.id}>
            {chat.image &&
            <Image
              src={'http://localhost:8000/uploads/' + chat.image}
              thumnail
              style={{width: '100px', marginRight: '10px'}}
            />
            }
            <Panel.Body>
              <Link to={'/chat/' + chat.id}>
                Author: {chat.author}
              </Link>
              <strong style={{marginLeft: '10px'}}>
                Message: {chat.message}
              </strong>
            </Panel.Body>
          </Panel>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    chat: state.chat.chat
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchChat: () => dispatch(fetchChat())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Chats);