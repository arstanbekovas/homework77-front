import {FETCH_CHAT_SUCCESS} from "../actions/actionTypes";

const initialState = {
  chat: []
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_CHAT_SUCCESS:
      return {...state, chat: action.chat};
    default:
      return state;
  }
};

export default reducer;