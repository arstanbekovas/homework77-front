import axios from '../../axios-api';
import {CREATE_CHAT_SUCCESS, FETCH_CHAT_SUCCESS} from "./actionTypes";

export const fetchChatSuccess = chat => {
  return {type: FETCH_CHAT_SUCCESS, chat};
};

export const fetchChat = () => {
  return dispatch => {
    axios.get('/chat').then(
      response => dispatch(fetchChatSuccess(response.data))
    );
  }
};

export const createChatSuccess = () => {
  return {type: CREATE_CHAT_SUCCESS};
};

export const createChat = chatData => {
  return dispatch => {
    return axios.post('/chat', chatData).then(
      response => dispatch(createChatSuccess())
    );
  };
};