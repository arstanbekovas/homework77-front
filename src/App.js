import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";

import Toolbar from './components/UI/Toolbar/Toolbar';
import Chats from "./containers/Chats/Chats";
import NewMessage from "./containers/NewMessage/NewMessage";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header><Toolbar/></header>
        <main className="container">
          <Switch>
            <Route path="/" exact component={Chats} />
            <Route path="/chat/new" exact component={NewMessage} />
          </Switch>
        </main>
      </Fragment>
    );
  }
}

export default App;
